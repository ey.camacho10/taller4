package uniandes.pruebas;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class GooglePlay {

	public static final String URL_1 = "https://play.google.com/store/apps/category/FINANCE/collection/topselling_paid";
	public static final String DOMINIO = "https://play.google.com";

	public static final String URL_2 = "https://play.google.com/store/apps/details?id=com.davivienda.daviplataapp";

	public static void main(String[] args) {
		try {
			Document document = Jsoup.connect(URL_1).timeout(0).get();
			Elements elementos = document.getElementsByAttributeValue("class", "card-click-target");
			Set<String> hrefs = new HashSet<>();
			elementos.stream().forEach(e -> {
				hrefs.add(DOMINIO+e.attr("href"));
			});
			funciones(hrefs);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void funciones(Set<String> hrefs) {
		try {
			List<String> descripcion = new ArrayList<>();
			List<String> nombres = new ArrayList<>();
			List<String> numRatings = new ArrayList<>();
			List<String> ratingPromedio = new ArrayList<>();
			List<String> cambiosRecientes = new ArrayList<>();
			List<String> ratingCinco = new ArrayList<>();
			List<String> ratingCuatro = new ArrayList<>();
			List<String> precios = new ArrayList<>();
			
			for (String url : hrefs) {
				Document doc = Jsoup.connect(url).timeout(0).get();
				descripcion.add(doc.select("[itemprop='description']").text());
				nombres.add(doc.select("[class='id-app-title']").text());
				numRatings.add(doc.select("[class='rating-count']").text());
				ratingPromedio.add(doc.select("[class='score']").text());
				cambiosRecientes.add(doc.select("[class='recent-change']").text());
				ratingCinco.add(doc.select("[class='rating-bar-container five']").text());
				ratingCuatro.add(doc.select("[class='rating-bar-container four']").text());
				precios.add(doc.select("[class='price buy id-track-click id-track-impression']").text().replaceAll("Gratis", "").replaceAll("Comprar", "").trim());
			}
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("Descripciones: ");
			for (String d : descripcion) {
				System.out.println(d);
			}
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("Nombres: ");
			for (String d : nombres) {
				System.out.println(d);
			}
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("Numero Ratings: ");
			for (String d : numRatings) {
				System.out.println(d);
			}
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("Rating Promedio: ");
			for (String d : ratingPromedio) {
				System.out.println(d);
			}
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("Cambios recientes: ");
			for (String d : cambiosRecientes) {
				System.out.println(d);
			}
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("Ratings con 5 estrellas: ");
			for (String d : ratingCinco) {
				System.out.println(d);
			}
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("Ratings con 4 estrellas: ");
			for (String d : ratingCuatro) {
				System.out.println(d);
			}
			System.out.println("-----------------------------------------------------------------------------------------");
			System.out.println("Precios: ");
			for (String d : precios) {
				System.out.println(d);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
